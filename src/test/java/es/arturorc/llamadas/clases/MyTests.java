package es.arturorc.llamadas.clases;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MyTests {
	CalculoFacturacion c;
	Llamada[] objetosllamada;
	
	@BeforeAll
    void beforeAll() {
		this.c = new CalculoFacturacion();
		this.objetosllamada = new Llamada[5];
    }
	
	@Test
	@DisplayName("validar formato de la cadena de llamadas - duracion incorrecta")
    void testcalcularFacturacionDeCadenaLlamadas_01() {		
		double real = c.calcularFacturacionDeCadenaLlamadas("000:01:07,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090");		
        assertEquals(-1, real);
    }
	
	@Test
	@DisplayName("validar formato de la cadena de llamadas - teléfono incorrecto")
    void testcalcularFacturacionDeCadenaLlamadas_02() {		
		double real = c.calcularFacturacionDeCadenaLlamadas("00:01:07,4050-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090");		
        assertEquals(-1, real);
    }
	
	@Test
	@DisplayName("validar resultado con llamadas de 3 teléfonos")
    void testfacturacion() {
		
		objetosllamada = DatosPrueba.obtenerArrayLlamadas();
		double real = c.facturacion(objetosllamada);
        assertEquals(9, real);
    }
	
	@Test
	@DisplayName("validar tiempo en segundos con duración de llamada")
    void testcalcularTiempoLlamadaPorLineaTelefono() {
		String duracion = "01:01:01";
		int real = c.calcularTiempoLlamadaPorLineaTelefono(duracion);		
		
        assertEquals(3661, real);
    }
	
	@Test
	@DisplayName("validar importe de llamada aplicando tarifa con el tiempo en segundos")
    void testcalcularImportePorLineaTelefono() {
		double real = c.calcularImportePorLineaTelefono(280);			
		
        assertEquals(8.4, real);
    }
	
	
}
