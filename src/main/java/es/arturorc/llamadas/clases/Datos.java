package es.arturorc.llamadas.clases;

public interface Datos {
	
	public static final double PRECIO_MENOS5MINUTOS = 0.03;
	public static final double PRECIO_MAS5MINUTOS = 1.50;
	public static final double VALIDACION_CADENA_KO = -1;
	public static final String FORMATO_DURACION_LLAMADA = "[0-9][0-9]:[0-9][0-9]:[0-9][0-9]";
	public static final String FORMATO_NUM_TELEFONO = "[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9]";
}


