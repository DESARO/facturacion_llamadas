package es.arturorc.llamadas.clases;

public class Llamada implements Comparable<Llamada> {
	
	private String duracion;
	private String numeroTelefono;	
	private int duracionSegundos;
	
	public Llamada() {		
	}	

	public Llamada(String duracion, String numeroTelefono, int duracionSegundos) {
		super();
		this.duracion = duracion;
		this.numeroTelefono = numeroTelefono;
		this.duracionSegundos = duracionSegundos;
	}

	public int getDuracionSegundos() {
		return duracionSegundos;
	}

	public void setDuracionSegundos(int duracionSegundos) {
		this.duracionSegundos = duracionSegundos;
	}	

	public String getDuracion() {
		return duracion;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	
	@Override
	public String toString() {
		return "Llamada [duracion=" + duracion + ", numeroTelefono=" + numeroTelefono + ", duracionSegundos="
				+ duracionSegundos + "]";
	}

	@Override
	public int compareTo(Llamada o) {
		return this.numeroTelefono.compareTo(o.getNumeroTelefono());
	}	
	
}
