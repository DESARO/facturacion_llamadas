package es.arturorc.llamadas.clases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.*;  

public class CalculoFacturacion {

	public CalculoFacturacion() {
		
	}
	
	/* función que valida la cadena de llamadas usando expresiones regulares */
	public double calcularFacturacionDeCadenaLlamadas(String logLlamadas) {
		
		boolean validacionOK;		
		String duracionLlamada;
		String telefono;
		char c=(char)10;
		String separador=String.valueOf(c); 		
        String[] llamadas = logLlamadas.split(separador);
        Llamada[] objetosllamada = new Llamada[llamadas.length];
        double facturaFinal = 0;
        
        for (int i = 0; i < llamadas.length; i++) {
			duracionLlamada = llamadas[i].substring(0, 8); 
			telefono = llamadas[i].substring(9, 20); 
			validacionOK = Pattern.matches(Datos.FORMATO_DURACION_LLAMADA, duracionLlamada); 
			if(!validacionOK) {
				return Datos.VALIDACION_CADENA_KO;
			}
			validacionOK = Pattern.matches(Datos.FORMATO_NUM_TELEFONO, telefono);
			if(!validacionOK) {
				return Datos.VALIDACION_CADENA_KO;
			}
			objetosllamada[i] = new Llamada(duracionLlamada, telefono, 0);
		}        
        facturaFinal = this.facturacion(objetosllamada);
        return facturaFinal;
	}
	
	public double facturacion(Llamada[] llamadas){

		/*ordeno por teléfono para poder acumular la duración de sus llamadas */
		Arrays.sort(llamadas);
		/* creo un arraylist porque no se que tamaño va a tener */
		ArrayList<Llamada> llamadasPorNumero = new ArrayList<Llamada>();
		String aux = "";		
		int duracionTelefono=0;		
		
		for (int i = 0; i < llamadas.length;) {
			duracionTelefono = 0;
			aux = llamadas[i].getNumeroTelefono();
			while( i < llamadas.length && llamadas[i].getNumeroTelefono().equals(aux) )			{			   
			   duracionTelefono += calcularTiempoLlamadaPorLineaTelefono(llamadas[i].getDuracion());
			   i++;			   	
			} 			
			Llamada numero = new Llamada(llamadas[i-1].getDuracion(), llamadas[i-1].getNumeroTelefono(), duracionTelefono);
			llamadasPorNumero.add(numero);
		}
		
		/*invoco a la función para quitar de la factura al teléfono con mayor duración de llamadas */
		quitarDeFacturaTelefonoConLlamadasMasLargas( llamadasPorNumero );
				
		Llamada[] llamadasPorNumeroArray = new Llamada[llamadasPorNumero.size()];
		llamadasPorNumeroArray = llamadasPorNumero.toArray(llamadasPorNumeroArray);		
		
		/*invoco a la función que se recorrerá el array resultante para devolver el resultado de la facturación */
		double facturafinal = precioFinalTodasLlamadas(llamadasPorNumeroArray);
		return facturafinal;
    
	}

	/* función que calcula la duración en segundos de las llamadas en función de su duración en formato hh:mm:ss */
	public int calcularTiempoLlamadaPorLineaTelefono(String duracion) {
		
		String[] digitos =  duracion.split(":");		
		int horasASegundos = Integer.parseInt(digitos[0])*3600;
		int MinutosASegundos = Integer.parseInt(digitos[1])*60;
		int Segundos = Integer.parseInt(digitos[2]);
		int duracionSegundos = horasASegundos + MinutosASegundos + Segundos;
		return duracionSegundos;
	}
	
	/* función que calcula el importe de una llamada en función de su duuración en segundos  */
	public double calcularImportePorLineaTelefono(int duracionEnSegundos) {
		int duracionEnMinutos = 0;
		double importePorLinea = 0; 
		if(duracionEnSegundos < 300) {
			importePorLinea = duracionEnSegundos*Datos.PRECIO_MENOS5MINUTOS;
		}else {
			duracionEnMinutos = duracionEnSegundos/60;
			importePorLinea = duracionEnMinutos*Datos.PRECIO_MAS5MINUTOS;
		}
		return importePorLinea;
	}
	
	/* función que se recorrerá el array resultante  para devolver el resultado de la facturación */
	public double precioFinalTodasLlamadas(Llamada[] llamadasPorTelefono){
		double precioFinalFactura = 0; 
		for (int i = 0; i < llamadasPorTelefono.length; i++) {
			precioFinalFactura += calcularImportePorLineaTelefono(llamadasPorTelefono[i].getDuracionSegundos());
		}
		return precioFinalFactura;
	}
	
	/* función que quita de la facturación el teléfomo con mayor duración en llamadas */
	public void quitarDeFacturaTelefonoConLlamadasMasLargas(ArrayList<Llamada> llamadasPorNumero) {
		/* se ordena teléfono por duracion de sus llamadas en segundos de mayor a menor y quitamos el primer elemento */ 
		Collections.sort(llamadasPorNumero, new Comparator<Llamada>() {
		    @Override
		    public int compare(Llamada o1, Llamada o2) {
		        return o2.getDuracionSegundos() - o1.getDuracionSegundos();		            
		    }
		});
		
		for (Llamada llamada : llamadasPorNumero) {
			System.out.println(llamada.toString());
		}	
		llamadasPorNumero.remove(0);
		
	}

}
